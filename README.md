# IIT KGP FOUNDATION OF AIML COURSE VIDEOS

## VIDEO SET 6

| SL# | TOPICS |
|:---:| :----- |
|  1  | Scalable Data Mining Part 1 |
|  2  | Scalable Data Mining Part 2 |
|  3  | Text & Language Processing Part 1 |
|  4  | Text & Language Processing Part 2 |

